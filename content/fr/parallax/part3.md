---
title: "Retrouvez nous à Bouaké !"
date: 2020-07-09T16:41:42Z
draft: false
weight: 30
sitemapExclude: true
---

Nous sommes faciles à trouver.  Google peut vous diriger.

{{< button "Allons chez Bako!" "https://www.google.com/maps/dir/Current+Location/7.70879,-5.00619" >}}

ou bien:

Prenez la route vers Belleville,  Nous sommes à Belleville 1 au carréfour de l'ancien commisariat.
