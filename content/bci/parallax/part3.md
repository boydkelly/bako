---
title: "Retrouvez nous à Bouaké !"
date: 2020-07-09T16:41:42Z
draft: false
weight: 30
sitemapExclude: true
---

Nous sommes faciles à trouver.   

Cliquer sur le bouton pour les directions direct de Goolge Maps.

{{< button "Allons chez Bako!" "https://www.google.com/maps/dir/Current+Location/7.70879,-5.00619" >}}

ou bien:

Aller sur la route vers Belleville,  Nous sommes à Belleville 1 au carréfour de l'ancien commisariat.

